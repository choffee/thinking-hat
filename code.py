import board
import digitalio
import time

from adafruit_circuitplayground.express import cpx
#import adafruit_fancyled.adafruit_fancyled as fancy


cpx.pixels.auto_write = False  # Refresh pixels only when we say
cpx.pixels.brightness = 1.0    # We'll use FancyLED's brightness controls

# Declare a 4-element color palette, this one happens to be a
# 'blackbody' palette -- good for heat maps and firey effects.
#palette = [fancy.CRGB(1.0, 1.0, 1.0), # White
#           fancy.CRGB(1.0, 1.0, 0.0), # Yellow
#           fancy.CRGB(1.0, 0.0, 0.0), # Red
#           fancy.CRGB(0.0, 0.0, 0.0)] # Black

offset = 0  # Positional offset into color palette to get it to 'spin'
levels = (0.25, 0.3, 0.15)  # Color balance / brightness for gamma function

has_idea = 0
idea_target = 20
new_idea = True

cpx.play_file("COIN.WAV")
while True:
    acc_x, acc_y, acc_z = cpx.acceleration
    if acc_x < -1 and acc_x > -4 and acc_y < -3 and acc_y > -5:
        #color = fancy.palette_lookup(palette, acc_x / 10)
        #color = fancy.gamma_adjust(color, brightness=levels).pack()
        cpx.red_led = True
        if has_idea < idea_target + 10:
            has_idea += 1
    else:
        cpx.red_led = False
        if has_idea > 0:
            has_idea -= 1
    if has_idea >= idea_target:
        color = (50, 50, 50)
        if new_idea:
            cpx.play_file("Coin.wav")
            new_idea = False
    else:
        color = (0, 0, 0)
        new_idea = True
    for i in range(10):
        cpx.pixels[i] = color
        cpx.pixels.show()
    time.sleep(0.1)
