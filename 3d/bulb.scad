translate([0,0,30]){
    import("bulb/bulb.STL", 1);

translate([10,35,0]){
rotate(a=180, v=[1,0,0]){
    import("bulb/base.STL",1);
}}}
translate([22,22,0]){
    difference() {
        union() {
           cylinder(r=22, h=20);
           cylinder(r=32, h=12);
           cylinder(r=40, h=2);
        }
        translate([0,0,-0.1]){
            cylinder(r=26, h=11);
        }
        translate([-5,25,-0.1]){
            cube([10,20,10]);
        }
        for(a=[0:5]){
            rotate(a*360/6+30, [0, 0, 1]){
            translate([0, 35, -0.1]){
                cylinder(r=3, h=5);
            }}
        }
        translate([-15,21,5]){
            #cube([30,7,30]);
        }
    }
}
